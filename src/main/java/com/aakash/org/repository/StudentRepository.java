package com.aakash.org.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aakash.org.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Integer>{
	
	Student findById(int id);
	

}

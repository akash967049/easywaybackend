package com.aakash.org.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aakash.org.entity.Course;

public interface CourseRepository extends JpaRepository<Course, Integer>{
	
	Course findById(int id);

}

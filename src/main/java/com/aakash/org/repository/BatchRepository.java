package com.aakash.org.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aakash.org.entity.Batch;

public interface BatchRepository extends JpaRepository<Batch, Integer> {

	
	Batch findById(int id);
}

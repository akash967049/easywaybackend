package com.aakash.org.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aakash.org.entity.Teacher;


public interface TeacherRepository extends JpaRepository<Teacher, Integer>{

	Teacher findById(int id);
}

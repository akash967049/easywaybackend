package com.aakash.org.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aakash.org.entity.StudentPersonalInformation;

public interface StudentPersonalInformationRepository extends JpaRepository<StudentPersonalInformation, Integer>{
	
	StudentPersonalInformation findById(int id);

}

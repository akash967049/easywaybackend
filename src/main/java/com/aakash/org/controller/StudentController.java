package com.aakash.org.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aakash.org.service.StudentService;

@RestController
@CrossOrigin
public class StudentController {

	
	@Autowired
	private StudentService studentService;
	
	@GetMapping("/welcome")
	public String welcome() {
		return "You are most welcome";
	}
}

package com.aakash.org;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.aakash.org.entity.Batch;
import com.aakash.org.entity.Course;
import com.aakash.org.entity.Student;
import com.aakash.org.entity.StudentPersonalInformation;
import com.aakash.org.entity.Teacher;
import com.aakash.org.repository.BatchRepository;
import com.aakash.org.repository.CourseRepository;
import com.aakash.org.repository.TeacherRepository;
import com.aakash.org.service.BatchService;
import com.aakash.org.service.CourseService;
import com.aakash.org.service.StudentPersonalInformationService;
import com.aakash.org.service.StudentService;


@EnableWebMvc
@SpringBootApplication
public class EasyWayBackendApplication implements CommandLineRunner{

	@Autowired
	private StudentService studentService;
	
	@Autowired
	private StudentPersonalInformationService studentPIS;
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private BatchService batchService;
	
	@Autowired
	private BatchRepository batchRepository;
	
	@Autowired
	private TeacherRepository teacherRepository;
	
	@Autowired
	private CourseRepository courseRepository;

	
	public static void main(String[] args) {
		SpringApplication.run(EasyWayBackendApplication.class, args);
	}
	
	@Override
	@Transactional
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
		//******************
		// test for One to One mapping 
		
//		StudentPersonalInformation info = new StudentPersonalInformation("Aakash", "Verma", "akash@gmail.com", "7570940648", "01-10-2000", "Kuchaya");
//		Student akash= new Student("akash", "akash123", "active");
//		studentService.addStudent(akash);
//		Student newakash = studentService.getStudentById(1);
//		System.out.println(akash.toString());
//		studentPIS.addStudentPI(info);
//		System.out.println(info.toString());
//		StudentPersonalInformation newinfo  = studentPIS.getStudentPIById(1);
//		newakash.setStudentPersonalInformation(newinfo);
//		studentService.updateStudent(newakash);
		
		//***************
		// test for one to many
//		
//		Batch batch = new Batch("Aplha", "Arjun Singh", 4);
//		Course course = new Course("Angular", "Manish Singh", 3, 45000);
//		Course course1 = new Course("Angular", "Manish Singh", 3, 45000);

//		
//		batchService.addBatch(batch);
//		courseService.addCourse(course);
//		courseService.addCourse(course1);

//		
//		Batch newBatch = batchService.getBatchById(batch.getId());
//		Course newCourse = courseService.getCourseById(course.getId());
//		
//		Student newStudent = studentService.getenrolled(1, 1);
////		System.out.println(newStudent.toString());
		
		Batch newBatch = batchService.getBatchById(1);
		System.out.println("Hello "+batchRepository.findById(newBatch.getId()).getStudents().toString());
		
		// Many to Many testing
		
//		Teacher teacher= new Teacher("Naveen", "Yadav", "naveen@gmail.com", "4548755926");
//		teacherRepository.save(teacher);
//		Teacher teacher1= new Teacher("Ramesh", "Singh", "ramesh@gmail.com", "4548755926");
//		teacherRepository.save(teacher1);
		
//		Teacher newTeacher = teacherRepository.findById(3);
//		Course newCourse = courseRepository.findById(2);
//		Course c = newTeacher.getCourses().remove(0);
//		System.out.println(c.toString());
//		newTeacher.getCourses().add(newCourse);
//		teacherRepository.save(newTeacher);
		//***************
		
	}

}

package com.aakash.org.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aakash.org.entity.Batch;
import com.aakash.org.entity.Course;
import com.aakash.org.repository.BatchRepository;
import com.aakash.org.repository.CourseRepository;

@Service
public class BatchService {

	@Autowired
	private BatchRepository batchRepository;
	
	@Autowired
	private CourseRepository courseRepository;
	
	// Add a new batch
	
	public Batch addBatch(Batch batch) {
		return batchRepository.save(batch);
	}
	
	// Get Batch info By Id 
	
	public Batch getBatchById(int id) {
		return batchRepository.findById(id);
	}
	
	
	// Delete a batch By Id
	
	public String deleteBatchById(int id) {
		batchRepository.deleteById(id);
		return "Batch removed at id |"+id;
	}
	
	// Update a batch
	
	public Batch updateBatch(Batch batch) {
		if(batch.getId()!=0) {
			return batchRepository.save(batch);
		}else {
			return null;
		}
	}
	
	// Assign Course to batch
	
	public Batch getenrolled(int courseId, int id) {
		Batch batch = batchRepository.findById(id);
		Course course = courseRepository.findById(id);
		batch.setCourse(course);
		return batchRepository.save(batch);
	}
	
	// Remove Course from batch
	
	public Batch getunenrolled(int id) {
		Batch batch = batchRepository.findById(id);
		batch.setCourse(null);
		return batchRepository.save(batch);
		
	}
	
}

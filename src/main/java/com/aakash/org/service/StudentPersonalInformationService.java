package com.aakash.org.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aakash.org.entity.StudentPersonalInformation;
import com.aakash.org.repository.StudentPersonalInformationRepository;


@Service
public class StudentPersonalInformationService {
	
	@Autowired
	private StudentPersonalInformationRepository studentPIR;
	
	
	//add new student Personal Information
	
	public StudentPersonalInformation addStudentPI(StudentPersonalInformation student) {
		return studentPIR.save(student);
	}
	
	// Get student Personal Information by id
	public StudentPersonalInformation getStudentPIById(int id) {
		return studentPIR.findById(id);
	}
	
	// delete a student Personal Information by id
	
	public String removeStudentPI(int id) {
		studentPIR.deleteById(id);
		return "Student Persoanl Information removed at id | "+id;
	}
	
	// Update student Personal Information record
	
	public StudentPersonalInformation updateStudentPI(StudentPersonalInformation studentPI) {
		if(studentPI.getId()!=0) {
			return studentPIR.save(studentPI);
		}else {
			return null;
		}
	}
	
}

package com.aakash.org.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aakash.org.entity.Batch;
import com.aakash.org.entity.Student;
import com.aakash.org.repository.BatchRepository;
import com.aakash.org.repository.StudentRepository;

@Service
public class StudentService {
	
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private BatchRepository batchRepository;
	
	
	//add new student
	
	public Student addStudent(Student student) {
		return studentRepository.save(student);
	}
	
	// Get Student by id
	public Student getStudentById(int id) {
		return studentRepository.findById(id);
	}
	
	// delete a student by id
	
	public String removeStudent(int id) {
		studentRepository.deleteById(id);
		return "Student remove at id | "+id;
	}
	
	// Update Student record
	
	public Student updateStudent(Student student) {
		if(student.getId()!=0) {
			return studentRepository.save(student);
		}else {
			return null;
		}
	}
	
	// Get enroll in a batch
	
	public Student getenrolled(int batchId, int id) {
		Batch batch = batchRepository.findById(batchId);
		Student student = studentRepository.findById(id);
		student.setBatch(batch);
		return studentRepository.save(student);
	}
	
	// Get unenrolled from batch
	
	public Student getunenrolled(int id) {
		Student student = studentRepository.findById(id);
		student.setBatch(null);
		return studentRepository.save(student);
		
	}

}

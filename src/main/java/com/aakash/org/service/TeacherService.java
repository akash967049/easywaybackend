package com.aakash.org.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aakash.org.entity.Course;
import com.aakash.org.entity.Teacher;
import com.aakash.org.repository.CourseRepository;
import com.aakash.org.repository.TeacherRepository;

@Service
public class TeacherService {

	@Autowired
	private TeacherRepository teacherRepository;
	
	@Autowired
	private CourseRepository courseRepository;
	
	// Add to Teachers
	
	public Teacher addTeacher(Teacher teacher) {
		return teacherRepository.save(teacher);
	}
	
	// Get teacher By id
	public Teacher getTeacherById(int id) {
		return teacherRepository.findById(id);
	}
	
	// remove teacher by id
	
	public String removeTeacher(int id) {
		teacherRepository.deleteById(id);
		return "Teacher removed at | "+id;
	}
	
	// add course to teacher
	@Transactional
	public String addTeacherToCourse(int teacherId, int courseId) {
		Teacher newTeacher = teacherRepository.findById(teacherId);
		Course newCourse = courseRepository.findById(courseId);
		newTeacher.getCourses().add(newCourse);
		teacherRepository.save(newTeacher);
		return "Teacher added to course";
	}
	
	// Remove teacher from course
	@Transactional
	public String removeTeacherFromCourse(int teacherId, int courseId) {

		Teacher newTeacher = teacherRepository.findById(teacherId);
		Course newCourse = courseRepository.findById(courseId);
		int index = 0;
		for(Course c:newTeacher.getCourses()) {
			if(c.getId()==courseId) {
				break;
			}
			index+=1;
		}
		newTeacher.getCourses().remove(index);
		teacherRepository.save(newTeacher);
		return "Teacher is removed from the course";
		
	}
}

package com.aakash.org.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aakash.org.entity.Course;
import com.aakash.org.repository.CourseRepository;

@Service
public class CourseService {

	@Autowired
	private CourseRepository courseRepository;

	
	// Add a course

	public Course addCourse(Course course) {
		return courseRepository.save(course);
	}

	// Get Course info

	public Course getCourseById(int id) {
		return courseRepository.findById(id);
	}

	// Delete a course

	public String removeCourseById(int id) {
		courseRepository.deleteById(id);
		return "Course removed at id | " + id;
	}

	// Update a course

	public Course updateCourse(Course course) {
		if (course.getId() != 0) {
			return courseRepository.save(course);
		} else {
			return null;
		}
	}

}

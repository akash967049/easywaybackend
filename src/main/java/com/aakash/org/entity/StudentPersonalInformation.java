package com.aakash.org.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "studentpersonalinformation")
public class StudentPersonalInformation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String dateOfBirth;
	private String address;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id")
	private Student student;

	public StudentPersonalInformation(String firstName, String lastName, String email, String phone,
			String dateOfBirth, String address) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
	}

	@Override
	public String toString() {
		return "StudentPersonalInformation [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", phone=" + phone + ", dateOfBirth=" + dateOfBirth + ", address=" + address
				+ ", student=" + student + "]";
	}
	
	
}

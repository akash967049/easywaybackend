package com.aakash.org.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "batches")
public class Batch {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	
	private String name;
	private String coordinator;
	private int roomNumber;
	
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "course_id")
	private Course course;
	
	
	@OneToMany(mappedBy = "batch", cascade = {CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH})
	private List<Student> students= new ArrayList<>();


	public Batch(String name, String coordinator, int roomNumber) {
		super();
		this.name = name;
		this.coordinator = coordinator;
		this.roomNumber = roomNumber;
	}


	@Override
	public String toString() {
		return "Batch [id=" + id + ", name=" + name + ", coordinator=" + coordinator + ", roomNumber=" + roomNumber
				+ ", course=" + course + "]";
	}
	
	
}

package com.aakash.org.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "courses")
public class Course {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String name;
	private String headofcourse;
	
	@Column(name = "duaration_in_months")
	private int duration;
	
	private int fee;
	
	@OneToMany(mappedBy = "course", cascade = {CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.DETACH, CascadeType.REFRESH})
	private List<Batch> batches= new ArrayList<>();

	
	@ManyToMany(cascade = {
			CascadeType.PERSIST,
			CascadeType.MERGE
	}, fetch = FetchType.EAGER, mappedBy = "courses")
	private List<Teacher> teachers = new ArrayList<>();
	
	
	public Course(String name, String headofcourse, int duration, int fee) {
		super();
		this.name = name;
		this.headofcourse = headofcourse;
		this.duration = duration;
		this.fee = fee;
	}


	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", headofcourse=" + headofcourse + ", duration=" + duration
				+ ", fee=" + fee + "]";
	}
	
	
	
	
	
	

}
